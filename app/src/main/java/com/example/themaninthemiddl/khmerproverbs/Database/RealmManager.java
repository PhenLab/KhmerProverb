package com.example.themaninthemiddl.khmerproverbs.Database;

import com.example.themaninthemiddl.khmerproverbs.ultility.ApplicationLoader;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmManager {

    private static final int REALM_DATABASE_VERSION = 1;
    private static final String REALM_DATABASE_NAME = "i_learn_chinese";
    public static RealmManager instance;
    public static RealmConfiguration realmConfiguration;
    public static RealmManager getInstance(){

        if (instance == null){
            instance = new RealmManager();
        }
        return instance;
    }

    private RealmManager() {

        Realm.init(ApplicationLoader.applicationContext);
        realmConfiguration = geRealmConfig();

        boolean success = Realm.compactRealm(realmConfiguration);
        System.out.println("Realm Database Compact Result:"+success);
    }

    private RealmConfiguration geRealmConfig() {

        return new RealmConfiguration.Builder()
                .name(REALM_DATABASE_NAME)
                .schemaVersion(REALM_DATABASE_VERSION)
                .migration(new Migration())
                .build();
    }


    public Realm getNewRealm() {
        return Realm.getInstance(realmConfiguration);
    }
}

package com.example.themaninthemiddl.khmerproverbs.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.example.themaninthemiddl.khmerproverbs.MainActivity;
import com.example.themaninthemiddl.khmerproverbs.ultility.AndroidUtilities;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class SplashActivity extends AppCompatActivity {
    private String TAG = SplashActivity.class.getName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static Intent getIntent(Context context) {
        return new Intent(context, SplashActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        AndroidUtilities.RunOnThreadDelay(new Runnable() {
            @Override
            public void run() {
                startActivity(MainActivity.getIntent(getApplication()));
                finish();
            }
        },2000);
    }


    //register GCM and check google plat service
}

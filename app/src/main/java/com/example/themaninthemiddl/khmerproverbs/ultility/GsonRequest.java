package com.example.themaninthemiddl.khmerproverbs.ultility;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by User on 12/5/2015.
 */
public class GsonRequest<T> extends Request<T> {
    private Gson gson = new Gson();
    private final Class<T> clazz;
    private Map<String, String> param, header;
    private final Response.Listener<T> listener;
    private final Response.ErrorListener errorListener;

    public GsonRequest(String url, Class<T> clazz, Map<String, String> param, Response.Listener<T> listener, Response.ErrorListener errlistener) {
        super(Method.POST, url, errlistener);
        this.clazz = clazz;
        this.param = param;
        this.listener = listener;
        errorListener = errlistener;
    }

    public GsonRequest(String url, Class<T> clazz, int method, Map<String, String> param, Response.Listener<T> listener, Response.ErrorListener errlistener) {
        super(method, url, errlistener);
        this.clazz = clazz;
        this.param = param;
        this.listener = listener;
        errorListener = errlistener;
    }

    public GsonRequest(String url, Class<T> clazz, int method, Map<String, String> param, Map<String, String> header, Response.Listener<T> listener, Response.ErrorListener errlistener) {
        super(method, url, errlistener);
        this.clazz = clazz;
        this.param = param;
        this.header = header;
        this.listener = listener;
        errorListener = errlistener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return header != null ? header : super.getHeaders();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            Log.d("response", response.statusCode + "");
            String json = new String(response.data, "UTF-8");
            //String json=new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Log.i("json", json);
            return Response.success(gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            Log.e("Error1", e.getMessage() + "");
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            Log.e("Error Syntax Json", e.getMessage() + "");
            return Response.error(new ParseError(e));
        } catch (Exception ex) {
            Log.e("Error ", String.valueOf(ex.getMessage()));
            return Response.error(new ParseError(ex));
        }
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        Log.i("GetParam", "True");
        if (param == null)
            return super.getParams();
        else {
            Log.i("Return Param", "True");
            return param;
        }
    }

    @Override
    protected void deliverResponse(T response) {
        if (response != null) {
            //deliverResponse(response);
            listener.onResponse(response);

        } else {
            errorListener.onErrorResponse(new VolleyError("No listener attached"));
            //VolleyLog.wtf("No listener attached with %s class", this.getClass().getName());
        }

    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
        NetworkResponse networkResponse = error.networkResponse;
        if (networkResponse != null && networkResponse.statusCode == 401) {
            Log.d("GsonRequest err", networkResponse.statusCode + "");
//            AndroidUtilities.RunOnUIThread(() -> NotificationCenter.getInstance().postNotificationName(NotificationCenter.Unauthenticated));
        }
        Log.e("GsonRequest", "deliverError:" + error.getMessage());
    }
}

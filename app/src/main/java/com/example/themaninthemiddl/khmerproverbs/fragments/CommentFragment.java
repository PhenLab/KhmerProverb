package com.example.themaninthemiddl.khmerproverbs.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.themaninthemiddl.khmerproverbs.ultility.Validation;
import com.example.themaninthemiddl.khmerproverbs.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommentFragment extends Fragment implements View.OnClickListener {


    private EditText edtEmail, edtComment;

    private Typeface tittleFont;
    private AppCompatButton btnSend;
    private Validation validation;
    private FirebaseFirestore firestore;
    Map<String,Object> comment = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_comment, container, false);

        edtEmail = (EditText) view.findViewById(R.id.email);
        edtComment = (EditText) view.findViewById(R.id.comment);

        btnSend = (AppCompatButton) view.findViewById(R.id.send);
        tittleFont = Typeface.createFromAsset(getActivity().getAssets(),"fonts/KhmerOS.ttf");

        edtEmail.setTypeface(tittleFont);
        edtComment.setTypeface(tittleFont);

        ((TextInputLayout) view.findViewById(R.id.tl_email)).setTypeface(tittleFont);
        ((TextInputLayout) view.findViewById(R.id.tl_comment)).setTypeface(tittleFont);

        btnSend.setOnClickListener(this);
        btnSend.setTypeface(tittleFont);

        firestore = FirebaseFirestore.getInstance();

        return view;


    }


    @Override
    public void onClick(View view) {

        if (validation.isNull(edtEmail.getText().toString()))
            Toast.makeText(getActivity(), getString(R.string.input_error), Toast.LENGTH_SHORT).show();

        else if(validation.checkIfEmailValid(edtEmail.getText().toString())){

            try {

                if (!edtComment.getText().toString().equals("") || !edtComment.getText().toString().equals(null) ){


                    comment.put("email",edtEmail.getText().toString());
                    comment.put("comment",edtComment.getText().toString());

                    firestore.collection("comments")
                            .document(getRandomString())
                            .set(comment)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getActivity(), getString(R.string.comment_successul), Toast.LENGTH_SHORT).show();

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getActivity(), getString(R.string.comment_fail), Toast.LENGTH_SHORT).show();

                                }
                            });

                }
                else {
                    Toast.makeText(getActivity(), getString(R.string.input_comment), Toast.LENGTH_SHORT).show();

                }
            }
            catch (Exception e){

                Toast.makeText(getActivity(), getString(R.string.edit_error), Toast.LENGTH_SHORT).show();

            }
        }
        else {
            Toast.makeText(getActivity(), getString(R.string.gmail_error), Toast.LENGTH_SHORT).show();
        }
    }



    public String getRandomString() {

        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("EE,d MMMM yyyy h:mma", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        String formattedDate = sdf.format(date);
        Log.i("get_mili","mili"+System.currentTimeMillis()+","+formattedDate);
        return String.valueOf(System.currentTimeMillis());

    }


}

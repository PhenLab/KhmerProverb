package com.example.themaninthemiddl.khmerproverbs.ultility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by imac on 8/31/18.
 */

public class Validation {

    public static boolean checkIfEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public static boolean isNull(String value) {
        return value == null || value.isEmpty() || value.trim().isEmpty();
    }
}

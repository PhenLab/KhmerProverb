package com.example.themaninthemiddl.khmerproverbs.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.themaninthemiddl.khmerproverbs.utilse.DictionaryObject;


/**
 * Created by lotphen on 9/2/17.
 */

public class RecordData extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="dictionaryRecord";
    private static final String TABLE_HISTORY="history";
    private static final String TABLE_FAVORITE="favorite";

    //table
    private static final String KEY_ID="id";
    private static final String KEY_WORD="word";
    private static final String KEY_DEFINITION="definition";
    private static final String KEY_DETAIL="detail";
    private static final String KEY_DIRECT="direct";
    private static final String KEY_INDIRECT="indirect";

    public RecordData(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_TABLE_HISTORY="CREATE TABLE"+TABLE_HISTORY+"("+
                KEY_ID+"INTERGER PRIMARY KEY"+
                KEY_WORD+"TEXT"+")";
        sqLiteDatabase.execSQL(CREATE_TABLE_HISTORY);

        String CREATE_TABLE_FAVORITE="CREATE TABLE"+TABLE_FAVORITE+"("+
                KEY_ID+"INTERGER PRIMARY KEY"+
                KEY_WORD+"TEXT"+")";
        sqLiteDatabase.execSQL(CREATE_TABLE_FAVORITE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,int oldVersion, int newVersion) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXIST"+TABLE_HISTORY);
            sqLiteDatabase.execSQL("DROP TABLE IF EXIST"+TABLE_FAVORITE);
            onCreate(sqLiteDatabase);
    }

    //add new history
    public void addHistory(DictionaryObject dictionaryObject){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(KEY_WORD, dictionaryObject.getWord());
        db.insert(TABLE_HISTORY,null,contentValues);
        db.close();
    }
    //readHistory
    public void readHistory(){
    }
    //delete history
    public void deleteHistory(){
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL("DELETE FROM"+TABLE_HISTORY);
    }
}

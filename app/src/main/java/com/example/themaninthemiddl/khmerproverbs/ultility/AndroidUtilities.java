package com.example.themaninthemiddl.khmerproverbs.ultility;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import static android.content.ContentValues.TAG;

public class AndroidUtilities {

    public static void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        try {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm == null || !imm.isActive()) {
                return;
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + "");
        }
    }

    public static void RunOnUIThread(Runnable runnable){
        ApplicationLoader.applicationHandler.post(runnable);
    }
    public static void RunOnThreadDelay(Runnable runnable, long delayMiliSec){
        ApplicationLoader.applicationHandler.postDelayed(runnable, delayMiliSec);
    }
}

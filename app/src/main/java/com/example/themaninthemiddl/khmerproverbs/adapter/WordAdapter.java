package com.example.themaninthemiddl.khmerproverbs.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.themaninthemiddl.khmerproverbs.R;
import com.example.themaninthemiddl.khmerproverbs.activities.DetailActivity;
import com.example.themaninthemiddl.khmerproverbs.utilse.DictionaryObject;

import java.util.List;

/**
 * Created by lotphen on 8/18/17.
 */

public class WordAdapter extends RecyclerView.Adapter<WordAdapter.ViewHolder> {

    public List<DictionaryObject> data;
    public String table_name ="";

    public void setData(List<DictionaryObject>data, String table_name){
        this.data=data;
//        Log.d("look",this.data.get(0)+"");
        this.table_name = table_name;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context=parent.getContext();
        LayoutInflater inflater= LayoutInflater.from(context);

        //inflate layout
        View wordView=inflater.inflate(R.layout.word_view_holder,parent,false);


        //
        ViewHolder viewHolder=new ViewHolder(wordView,context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DictionaryObject dictionaryObject =data.get(position);
        holder.textViewWord.setText(dictionaryObject.getWord());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public Context context;
        public TextView textViewWord;

        public ViewHolder(View itemView, final Context context) {
            super(itemView);
            this.context=context;
            textViewWord=(TextView) itemView.findViewById(R.id.word);
            Typeface normalFont = Typeface.createFromAsset(context.getAssets(), "fonts/KhmerOS.ttf");
            textViewWord.setTypeface(normalFont,Typeface.NORMAL);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int positon=getAdapterPosition();
                    DictionaryObject dictionaryObject =data.get(positon);
                    Intent intent=new Intent(context, DetailActivity.class);
                    intent.putExtra("word", dictionaryObject.getWord());
                    intent.putExtra("definition", dictionaryObject.getDefinition());
                    intent.putExtra("detail", dictionaryObject.getDetail());
                    intent.putExtra("direct", dictionaryObject.getDirect());
                    intent.putExtra("indirect", dictionaryObject.getIndirect());
                    intent.putExtra("is_favorite", dictionaryObject.getIs_favorite());
                    intent.putExtra("id", dictionaryObject.getId());
                    intent.putExtra("table_name", table_name);
                    intent.putExtra("is_add", dictionaryObject.getIs_add());


                    context.startActivity(intent);
                }
            });

        }
    }
}

package com.example.themaninthemiddl.khmerproverbs.fragments;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toolbar;

import com.example.themaninthemiddl.khmerproverbs.Database.DbBackend;
import com.example.themaninthemiddl.khmerproverbs.R;
import com.example.themaninthemiddl.khmerproverbs.adapter.WordAdapter;
import com.example.themaninthemiddl.khmerproverbs.ultility.AndroidUtilities;
import com.example.themaninthemiddl.khmerproverbs.utilse.DictionaryObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private WordAdapter wordAdapter = new WordAdapter();
    private List<DictionaryObject> data;
    private DbBackend dbBackend;
    private RecyclerView recyclerView;
    private SearchView searchView;
    private String table_name= "dictionary";
    private TextView tvNoData;
    Toolbar toolbar_searchview;
    private boolean isFirstOpen = true;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragemnetView = inflater.inflate(R.layout.fragment_home, container, false);

        data=new ArrayList<>();
        dbBackend=new DbBackend(getActivity());
        data= dbBackend.dictionaryWords();

        recyclerView=(RecyclerView)fragemnetView.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));



        wordAdapter.setData(data,table_name);
        recyclerView.setAdapter(wordAdapter);

        searchView =(SearchView)fragemnetView.findViewById(R.id.searchdata);

        Typeface textFont = Typeface.createFromAsset(getActivity().getAssets(),"fonts/KhmerOS.ttf");

        TextView searchText = (TextView)
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setTypeface(textFont);
        tvNoData = (TextView)fragemnetView.findViewById(R.id.tvNoData);
        openOrCloseRecycler(data);
        tvNoData.setTypeface(textFont);

        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchData(newText);
                return false;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    Log.i("focus_change","true");
                    closeKeyboardWindow();
                    showInputMethod(view.findFocus());
                }
            }
        });

        container.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                container.getWindowVisibleDisplayFrame(r);
                int screenHeight = container.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d("keyboar", "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    Log.i("check_if_has","Software Keyboard was shown");
                    if(isFirstOpen) {

//                        closeKeyboardWindow();
                        isFirstOpen = false;
                    }
                }
                else {
                    // keyboard is closed
                    Log.i("check_if_no","Software Keyboard was hide");

                }
            }
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                AndroidUtilities.hideKeyboard(recyclerView);
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        return fragemnetView;


    }


    private void searchData(String newText) {
        List<DictionaryObject>temp=new ArrayList<>();
        DbBackend dbBackend=new DbBackend(getActivity());
        data=dbBackend.dictionaryWords();
        for(DictionaryObject dictionaryObject :data){
            if(dictionaryObject.getWord().contains(newText)){
                temp.add(dictionaryObject);
            }
        }
        data=temp;
        openOrCloseRecycler(data);
        wordAdapter.setData(data, table_name);
    }

    public void openOrCloseRecycler(List<DictionaryObject> data){

        if (data.size()>0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);

        }
        else {
            tvNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        Objects.requireNonNull(getActivity()).getMenuInflater().inflate(R.menu.menu_toolbar,menu);

        MenuItem menuItem = menu.findItem(R.id.search_menu);

        searchView = (SearchView) menuItem.getActionView();

        Log.i("check_search_view",searchView+"");

        super.onCreateOptionsMenu(menu, inflater);

    }

    public void closeKeyboardWindow() {
        InputMethodManager inputMethodManager =
                (InputMethodManager)getActivity().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        assert inputMethodManager != null;
        inputMethodManager.hideSoftInputFromWindow(
               getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    private void showInputMethod(View view) {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AndroidUtilities.hideKeyboard(searchView);

    }
}

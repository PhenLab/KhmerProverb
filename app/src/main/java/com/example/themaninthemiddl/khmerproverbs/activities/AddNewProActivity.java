package com.example.themaninthemiddl.khmerproverbs.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.telecom.Call;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.themaninthemiddl.khmerproverbs.Database.DbBackend;
import com.example.themaninthemiddl.khmerproverbs.R;

import java.util.ArrayList;

public class AddNewProActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView toolbarTitle;
    private Toolbar toolbar;
    ArrayList<String> record;
    private DbBackend dbBackend;
    private String db_table= "dictionary";
    private String is_add = "1";
    private String is_favorite = "0";




    private EditText edtDefinition, edtHeadDefinition, edtProverb;
    private EditText edtDetail, edtHeadDetail;
    private EditText edtDirect, edtHeadDirect;
    private EditText edtIndirect,edtHeadIndirect;
    private AppCompatButton btnSave;
    Typeface textFont;
    Typeface tittleFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_pro);


        toolbar= (Toolbar) findViewById(R.id.toolbar);
        tittleFont = Typeface.createFromAsset(getAssets(),"fonts/KhmerOSmuol.ttf");
        textFont = Typeface.createFromAsset(getAssets(),"fonts/KhmerOS.ttf");

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.add_proverb));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        applyFontForToolbarTitle(this);
        dbBackend = new DbBackend(this);

        //input

        edtProverb = (EditText)findViewById(R.id.edt_proverb);
        edtDefinition = (EditText)findViewById(R.id.edt_definition);
        edtDetail = (EditText)findViewById(R.id.edt_detail);
        edtDirect = (EditText)findViewById(R.id.edt_direct_meaning);
        edtIndirect = (EditText)findViewById(R.id.edt_indirect_meaning);
        btnSave = (AppCompatButton) findViewById(R.id.btn_save);

        btnSave.setOnClickListener(this);

        //apply hint text font
        applyFontForEditHint();



    }

    private void applyFontForEditHint() {

        edtProverb.setTypeface(textFont);
        edtDefinition.setTypeface(textFont);
        edtDetail.setTypeface(textFont); ;
        edtDirect.setTypeface(textFont); ;
        edtIndirect.setTypeface(textFont); ;
        btnSave.setTypeface(textFont); ;

        ((TextInputLayout)findViewById(R.id.tl_proverb)).setTypeface(textFont);
        ((TextInputLayout)findViewById(R.id.tl_definition)).setTypeface(textFont);
        ((TextInputLayout)findViewById(R.id.tl_detail)).setTypeface(textFont);
        ((TextInputLayout)findViewById(R.id.tl_direct)).setTypeface(textFont);
        ((TextInputLayout)findViewById(R.id.tl_indirect)).setTypeface(textFont);




    }

    public void applyFontForToolbarTitle(Activity context){
        Toolbar toolbar = (Toolbar) context.findViewById(R.id.toolbar);
        for(int i = 0; i < toolbar.getChildCount(); i++){
            View view = toolbar.getChildAt(i);
            if(view instanceof TextView){

                TextView tv = (TextView) view;
                if(tv.getText().equals(toolbar.getTitle())){
                    tv.setTypeface(tittleFont);
                    break;
                }

            }
        }
    }

    @Override
    public void onClick(View view) {

        //ad data to database

        if (!edtProverb.getText().toString().equals("") ) {

            if (!edtDetail.getText().toString().equals("")) {


                record = new ArrayList<>();
                record.add(edtProverb.getText().toString());
                record.add(edtDefinition.getText().toString());
                record.add(edtDetail.getText().toString());
                record.add(edtDirect.getText().toString());
                record.add(edtIndirect.getText().toString());
                record.add(is_favorite);
                record.add(is_add);

                String id = dbBackend.insertNewRecord(db_table, record);
                if (!id.equals("0")) {

                    Toast.makeText(this, getString(R.string.insert_success), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, DetailActivity.class);

                    intent.putExtra("id", id);

                    intent.putExtra("word", edtProverb.getText().toString());
                    intent.putExtra("definition", edtDefinition.getText().toString());
                    intent.putExtra("detail", edtDetail.getText().toString());
                    intent.putExtra("direct", edtDirect.getText().toString());
                    intent.putExtra("indirect", edtIndirect.getText().toString());
                    intent.putExtra("is_favorite", is_favorite);
                    intent.putExtra("id", id);
                    intent.putExtra("table_name", db_table);
                    intent.putExtra("is_add", is_add);

                    startActivity(intent);
                    this.finish();

                } else {
                    Toast.makeText(this, getString(R.string.insert_error), Toast.LENGTH_SHORT).show();

                }
            }
            else
                Toast.makeText(this, getString(R.string.insert_detail), Toast.LENGTH_SHORT).show();

        }
        else
            Toast.makeText(this, getString(R.string.insert_proverb), Toast.LENGTH_SHORT).show();


    }
}

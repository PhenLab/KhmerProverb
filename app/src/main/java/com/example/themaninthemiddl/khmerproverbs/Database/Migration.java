package com.example.themaninthemiddl.khmerproverbs.Database;

import android.util.Log;


import com.example.themaninthemiddl.khmerproverbs.utilse.DictionaryObject;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

class Migration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema realmSchema = realm.getSchema();

        if (oldVersion == 1 && realmSchema != null){

            Log.d("checkifreamlv1","true");
            if (realmSchema.contains("Category")){
                realmSchema.remove("Category");
            }
            realmSchema.createWithPrimaryKeyField("DictionaryObject","id", DictionaryObject.class)
                    .addField("word", String.class)
                    .addField("definition", String.class)
                    .addField("detail", String.class)
                    .addField("direct", String.class)
                    .addField("indirect", String.class)
                    .addField("is_add", String.class)
                    .addField("is_favorite", String.class);
        }
        else {
            Log.d("checkifreamlv",oldVersion+"");

        }


    }
}

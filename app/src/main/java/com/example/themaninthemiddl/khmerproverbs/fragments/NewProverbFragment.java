package com.example.themaninthemiddl.khmerproverbs.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.themaninthemiddl.khmerproverbs.Database.DbBackend;
import com.example.themaninthemiddl.khmerproverbs.R;
import com.example.themaninthemiddl.khmerproverbs.adapter.WordAdapter;
import com.example.themaninthemiddl.khmerproverbs.utilse.DictionaryObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewProverbFragment extends Fragment {


    private WordAdapter wordAdapter = new WordAdapter();
    private List<DictionaryObject> data;
    private DbBackend dbBackend;
    private RecyclerView recyclerView;
    private SearchView searchView;
    private String table_name= "dictionary";
    private TextView tvNoData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragemnetView =  inflater.inflate(R.layout.fragment_new_proverb, container, false);

        data=new ArrayList<>();
        dbBackend=new DbBackend(getActivity());
        data= dbBackend.getNewProverbs();

        recyclerView=(RecyclerView)fragemnetView.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        wordAdapter.setData(data, table_name);
        recyclerView.setAdapter(wordAdapter);

        searchView =(SearchView)fragemnetView.findViewById(R.id.searchdata);
        Typeface textFont = Typeface.createFromAsset(getActivity().getAssets(),"fonts/KhmerOS.ttf");

        TextView searchText = (TextView)
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setTypeface(textFont);
        tvNoData = (TextView)fragemnetView.findViewById(R.id.tvNoData);
        tvNoData.setTypeface(textFont);

        openOrCloseRecycler(data);
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.setIconifiedByDefault(false);

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchData(newText);
                return false;
            }
        });
//        Toast.makeText(this.getActivity(),data.size(),Toast.LENGTH_SHORT).show();
        return fragemnetView;
    }

    private void searchData(String newText) {
        List<DictionaryObject>temp=new ArrayList<>();
        DbBackend dbBackend=new DbBackend(getActivity());
        data=dbBackend.getNewProverbs();
        for(DictionaryObject dictionaryObject :data){
            if(dictionaryObject.getWord().contains(newText)){
                temp.add(dictionaryObject);
            }
        }
        data=temp;
        openOrCloseRecycler(data);
        wordAdapter.setData(data,  table_name);
    }

    public void openOrCloseRecycler(List<DictionaryObject> data){

        if (data.size()>0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);

        }
        else {
            tvNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }


}

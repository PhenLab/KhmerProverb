package com.example.themaninthemiddl.khmerproverbs.ultility;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Looper;
import android.support.multidex.MultiDex;
import android.util.Log;

public class ApplicationLoader extends Application {


    public static volatile Context applicationContext;
    public static volatile Handler applicationHandler;
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationContext = getApplicationContext();
        applicationHandler = new Handler(Looper.getMainLooper());

//        RealmManager.getInstance();

        //check app crash

        //initalize account
        Log.d("check_if_come","true");
        //initalize thread
//        AndroidUtility.RunOnUIThread()
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.e("ApplicationLoader","onConfigurationChanged");
    }

}

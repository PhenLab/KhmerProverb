package com.example.themaninthemiddl.khmerproverbs;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.themaninthemiddl.khmerproverbs.Database.DbBackend;
import com.example.themaninthemiddl.khmerproverbs.activities.AddNewProActivity;
import com.example.themaninthemiddl.khmerproverbs.font.CustomTypefaceSpan;
import com.example.themaninthemiddl.khmerproverbs.fragments.CommentFragment;
import com.example.themaninthemiddl.khmerproverbs.fragments.FavoriteFragment;
import com.example.themaninthemiddl.khmerproverbs.fragments.HomeFragment;
import com.example.themaninthemiddl.khmerproverbs.fragments.NewProverbFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private DrawerLayout drawerLayout;
    private FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private TextView toolbarTitle;
    private ImageView icAddNewPro, icFavorite, icSound;
    private DbBackend dbBackend;
    Toolbar toolbar_search_view;
    Toolbar toolbar;
    private boolean isFragmentShowing;
    SearchView searchView;
    String shareUrl = "https://drive.google.com/file/d/1Wu9CjO-A_y_TnDgIBZ_HgcpFAzGEukLA/view";
    private Runnable mPendingRunable;
    private Handler mHandler;
    private NavigationView navigationView;


    public static Intent getIntent(Context context){
        return new Intent(context, MainActivity.class);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Toobar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_search_view = (Toolbar)findViewById(R.id.toolbar_searchview);

        setSupportActionBar(toolbar_search_view);

        Typeface normalFont = Typeface.createFromAsset(getAssets(), "fonts/KhmerOS.ttf");


        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar_search_view,R.string.open,R.string.close);
        actionBarDrawerToggle.syncState();
        ActionBarDrawerToggle actionBarDrawerToggle1=new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        actionBarDrawerToggle1.syncState();
        dbBackend = new DbBackend(this);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View drawer, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

                if (mPendingRunable != null){
                    mHandler.post(mPendingRunable);
                    mPendingRunable = null;
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                Log.d("whenclosedrawer","true");

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        closeKeyboardWindow();
                    }
                });


            }
        });



        toolbarTitle = (TextView)findViewById(R.id.toolbar_title);
        icAddNewPro = (ImageView) findViewById(R.id.ic_add_new);
        icFavorite = (ImageView) findViewById(R.id.ic_favorite);
        icSound = (ImageView) findViewById(R.id.ic_sound);

        icAddNewPro.setOnClickListener(this);
        icFavorite.setOnClickListener(this);
        icSound.setOnClickListener(this);

        Typeface tittleFont = Typeface.createFromAsset(getAssets(),"fonts/KhmerOSmuol.ttf");

        toolbarTitle.setTypeface(tittleFont, Typeface.NORMAL);


        // Navigation View
        navigationView = (NavigationView) findViewById(R.id.navigationview);
        navigationView.setNavigationItemSelectedListener(this);

        // Actionbar Drawer
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        setDefaultFragment();
        setDefaultFragment();

//        navigationView.setCheckedItem(R.id.mnu_home);


        mHandler = new Handler(Looper.getMainLooper());
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/KhmerOSmuol.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    private void setIconGone(){
        icSound.setVisibility(View.GONE);
        icFavorite.setVisibility(View.GONE);
        icAddNewPro.setVisibility(View.GONE);

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {

        mPendingRunable = new Runnable() {
            @Override
            public void run() {
                switch (item.getItemId()){
                    case R.id.mnu_home:
                        toolbarTitle.setText(getString(R.string.home));
                        setDefaultFragment();
                        setIconGone();
                        break;

                    case R.id.mnu_new:
                        toolbarTitle.setText(getString(R.string.new_proverb));
                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.lyt_content, new NewProverbFragment());
                        fragmentTransaction.commit();
                        setIconGone();
                        icAddNewPro.setVisibility(View.VISIBLE);
                        break;

                    case R.id.mnu_favorite:
                        toolbarTitle.setText(getString(R.string.favorite_proverb));
                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.lyt_content, new FavoriteFragment());
                        fragmentTransaction.commit();
                        setIconGone();
                        icFavorite.setVisibility(View.VISIBLE);

                        break;
                    case R.id.mnu_share:
                        setIconGone();
                        shareTextUrl();

                        break;
                    case R.id.mnu_comment:
                        //change tool bar
                        toolbar.setVisibility(View.VISIBLE);
                        toolbar_search_view.setVisibility(View.GONE);

                        setIconGone();
                        toolbarTitle.setText(getString(R.string.comments));
                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.lyt_content, new CommentFragment());
                        fragmentTransaction.commit();
                        break;

                }

            }
        };
        drawerLayout.closeDrawers();
        return  true;
    }

    private void setDefaultFragment() {
        fragmentManager= getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.lyt_content,new HomeFragment());
        fragmentTransaction.commit();
    }

    public void closeKeyboardWindow() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getApplication().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        assert inputMethodManager != null;
        inputMethodManager.hideSoftInputFromWindow(
                Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
    }

    // Method to share either text or URL.
    private void shareTextUrl() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);

        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, shareUrl);
        startActivity(Intent.createChooser(intent, getString(R.string.share_with)));
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.ic_add_new:

                startActivity(new Intent(getApplication(), AddNewProActivity.class));
                break;

            case R.id.ic_favorite:

                dbBackend.changeAllFavoriteStatus();
                break;


            case R.id.ic_sound:

                break;
        }
    }





}

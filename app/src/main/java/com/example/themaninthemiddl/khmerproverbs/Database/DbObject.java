package com.example.themaninthemiddl.khmerproverbs.Database;

/**
 * Created by phen on 6/24/2017.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DbObject {

    private static DictionaryDatabase dbHelper;
    private SQLiteDatabase db;

    public DbObject(Context context) {

        dbHelper = new DictionaryDatabase(context);

        this.db = dbHelper.getReadableDatabase();
        this.db = dbHelper.getWritableDatabase();


    }

    public SQLiteDatabase getDbConnection(){

        return this.db;

    }

    public void closeDbConnection(){

        if(this.db != null){

            this.db.close();

        }
    }
    public void openDbConnection(){

        if(this.db != null){

            this.db.close();

        }
    }
}

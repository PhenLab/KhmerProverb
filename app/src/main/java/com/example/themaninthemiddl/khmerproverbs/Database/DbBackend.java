package com.example.themaninthemiddl.khmerproverbs.Database;

/**
 * Created by phen on 6/24/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;


import com.example.themaninthemiddl.khmerproverbs.utilse.DictionaryObject;

import java.util.ArrayList;
import java.util.List;

public class DbBackend extends DbObject{

    public DbBackend(Context context) {
        super(context);
    }

    public List<DictionaryObject> dictionaryWords(){
        String query = "Select * from dictionary order by word ASC ";
        List<DictionaryObject> data;
        Cursor cursor = this.getDbConnection().rawQuery(query, null);
        data = new ArrayList<>();

        int i =0;
        if(cursor.moveToFirst()){
            do{
                i+=1;
                String word = cursor.getString(cursor.getColumnIndexOrThrow("word"));
                String definiton=cursor.getString(cursor.getColumnIndexOrThrow("definition"));
                String detail=cursor.getString(cursor.getColumnIndexOrThrow("detail"));
                String direct=cursor.getString(cursor.getColumnIndexOrThrow("direct"));
                String indirect=cursor.getString(cursor.getColumnIndexOrThrow("indirect"));
                String id=cursor.getString(cursor.getColumnIndexOrThrow("id"));
                String is_favorite=cursor.getString(cursor.getColumnIndexOrThrow("is_favorite"));
                String is_add=cursor.getString(cursor.getColumnIndexOrThrow("is_add"));

                try {
                    if (word.equals("NULL") || word.equals("null") || word.equals(null) || word.equals("")){
                    }else {
                        Log.i("check_word", i + word);
                        data.add(new DictionaryObject(id,word, definiton,detail,direct,indirect,is_favorite,is_add));
                    }

                }catch (Exception e){

                }
            }while(cursor.moveToNext());
        }
        Log.i("check_data_size",data.size()+","+i);

        return data;
    }

    public List<DictionaryObject> getNewProverbs(){
        String query = "Select * from dictionary where is_add = 1 order by word ASC ";
        List<DictionaryObject> data;
        Cursor cursor = this.getDbConnection().rawQuery(query, null);
        data = new ArrayList<>();

        int i =0;
        if(cursor.moveToFirst()){
            do{
                i+=1;
                String word = cursor.getString(cursor.getColumnIndexOrThrow("word"));
                String definiton=cursor.getString(cursor.getColumnIndexOrThrow("definition"));
                String detail=cursor.getString(cursor.getColumnIndexOrThrow("detail"));
                String direct=cursor.getString(cursor.getColumnIndexOrThrow("direct"));
                String indirect=cursor.getString(cursor.getColumnIndexOrThrow("indirect"));
                String id=cursor.getString(cursor.getColumnIndexOrThrow("id"));
                String is_favorite=cursor.getString(cursor.getColumnIndexOrThrow("is_favorite"));
                String is_add=cursor.getString(cursor.getColumnIndexOrThrow("is_add"));

                try {
                    if (word.equals("NULL") || word.equals("null") || word.equals(null) || word.equals("")){
                    }else {
                        Log.i("check_word", i + word);
                        data.add(new DictionaryObject(id,word, definiton,detail,direct,indirect,is_favorite,is_add));
                    }

                }catch (Exception e){

                }
            }while(cursor.moveToNext());
        }
        Log.i("check_data_size",data.size()+","+i);

        return data;
    }

    public DictionaryObject getQuizById(int quizId){
        DictionaryObject dictionaryObject = null;
        String query = "select * from dictionary where id = " + quizId;
        Cursor cursor = this.getDbConnection().rawQuery(query, null);
        if(cursor.moveToFirst()){
            do{
                String word = cursor.getString(cursor.getColumnIndexOrThrow("word"));
                String meaning = cursor.getString(cursor.getColumnIndexOrThrow("definition"));
                String detail=cursor.getString(cursor.getColumnIndexOrThrow("detail"));
                String direct=cursor.getString(cursor.getColumnIndexOrThrow("direct"));
                String indirect=cursor.getString(cursor.getColumnIndexOrThrow("indirect"));
                String id=cursor.getString(cursor.getColumnIndexOrThrow("id"));
                String is_favorite=cursor.getString(cursor.getColumnIndexOrThrow("is_favorite"));
                String is_add=cursor.getString(cursor.getColumnIndexOrThrow("is_add"));

                dictionaryObject = new DictionaryObject(id,word, meaning,detail,direct,indirect,is_favorite,is_add);
            }while(cursor.moveToNext());

        }
        cursor.close();
        return dictionaryObject;
    }

    //get word Id
    public String getWordId(String word) {
        String query = "select * from dictionary where id=" +7;
        Cursor cursor = this.getDbConnection().rawQuery(query, null);
        int Id = 0;
        String definition = null;
        if (cursor.moveToFirst()) {
            do {
                definition = cursor.getString(cursor.getColumnIndexOrThrow("definition"));

            } while (cursor.moveToNext());
            Log.d("select:", "true");
        }
        cursor.close();
        return definition;
    }

    //get word Id
    public String insertNewRecord(String TABLE_NAME, ArrayList<String> record) {

        try {


            this.getDbConnection().isOpen();
            ContentValues cv = new ContentValues();
            cv.put("word", record.get(0));
            cv.put("definition", record.get(1));
            cv.put("detail", record.get(2));
            cv.put("direct", record.get(3));
            cv.put("indirect", record.get(4));
            cv.put("is_favorite", record.get(5));
            cv.put("is_add", record.get(6));

            Long id =  this.getDbConnection().insert(TABLE_NAME, null, cv);
            this.getDbConnection().close();

            return id.toString();
        }catch (Exception e){

            return "0";
        }
    }

    public List<DictionaryObject> getFavoriteList(){

        String query = "Select * from dictionary where dictionary.is_favorite = 1 order by word ASC ";
        List<DictionaryObject> data;
        Cursor cursor = this.getDbConnection().rawQuery(query, null);
        data = new ArrayList<>();

        int i =0;
        if(cursor.moveToFirst()){
            do{
                i+=1;
                String word = cursor.getString(cursor.getColumnIndexOrThrow("word"));
                String definiton=cursor.getString(cursor.getColumnIndexOrThrow("definition"));
                String detail=cursor.getString(cursor.getColumnIndexOrThrow("detail"));
                String direct=cursor.getString(cursor.getColumnIndexOrThrow("direct"));
                String indirect=cursor.getString(cursor.getColumnIndexOrThrow("indirect"));
                String id=cursor.getString(cursor.getColumnIndexOrThrow("id"));
                String is_favorite=cursor.getString(cursor.getColumnIndexOrThrow("is_favorite"));
                String is_add=cursor.getString(cursor.getColumnIndexOrThrow("is_add"));

                try {
                    if (word.equals("NULL") || word.equals("null") || word.equals(null) || word.equals("")){
                    }else {
                        Log.i("check_word", i + word);
                        data.add(new DictionaryObject(id,word, definiton, detail, direct, indirect,is_favorite,is_add));
                    }

                }catch (Exception e){

                }
            }while(cursor.moveToNext());
        }
        Log.i("check_data_size",data.size()+","+i);

        return data;
    }


    public void changeFavoriteStatus(String is_favorite, String table_name, String id){
//
//        String query = "update "+ table_name + "" +
//                        " set is_favorite = "+ is_favorite +"where id = "+ id;
        ContentValues contentValues = new ContentValues();
        contentValues.put("is_favorite",is_favorite);
        this.getDbConnection().update(table_name,contentValues,"id="+id,null);


    }

    public void changeAllFavoriteStatus(){

        ContentValues contentValues = new ContentValues();
        contentValues.put("is_favorite","0");

        int result = this.getDbConnection().update("dictionary",contentValues,null,null);

        Log.d("check_result",result+",");

    }


    public DictionaryObject getProverbById(String ID){
        String query = "Select * from dictionary where id = "+ID;
        DictionaryObject data = null;

        Cursor cursor = this.getDbConnection().rawQuery(query,null);
        Log.i("check_data_by_id",",");

        int i =0;
        if(cursor.moveToFirst()){
            Log.i("check_data_by_id",","+cursor.getString(cursor.getColumnIndexOrThrow("word")));

            do{
                String word = cursor.getString(cursor.getColumnIndexOrThrow("word"));
                String definiton=cursor.getString(cursor.getColumnIndexOrThrow("definition"));
                String detail=cursor.getString(cursor.getColumnIndexOrThrow("detail"));
                String direct=cursor.getString(cursor.getColumnIndexOrThrow("direct"));
                String indirect=cursor.getString(cursor.getColumnIndexOrThrow("indirect"));
                String id=cursor.getString(cursor.getColumnIndexOrThrow("id"));
                String is_favorite=cursor.getString(cursor.getColumnIndexOrThrow("is_favorite"));
                String is_add=cursor.getString(cursor.getColumnIndexOrThrow("is_add"));

                Log.i("check_data_by_id",word+","+is_favorite);
                i+=1;
                data = new DictionaryObject(id,word,definiton,detail,direct,indirect,is_favorite,is_add);


            }while(cursor.moveToNext());
        }
        cursor.close();
        return data;

    }

    public String deleteNewProverbById(String ID){
//        String query = "delete from dictionary where id = "+ID;

        try {
            this.getDbConnection().delete("dictionary", "id ="+ID, null);

            return "success";
        }catch (Exception e){

            return "error";
        }


    }

    public String updateProverb(String TABLE_NAME, ArrayList<String> record, String ID) {

        try {


            this.getDbConnection().isOpen();
            ContentValues cv = new ContentValues();
            cv.put("word", record.get(0));
            cv.put("definition", record.get(1));
            cv.put("detail", record.get(2));
            cv.put("direct", record.get(3));
            cv.put("indirect", record.get(4));
            cv.put("is_favorite", record.get(5));
            cv.put("is_add", record.get(6));

            int id =  this.getDbConnection().update(TABLE_NAME, cv,"id="+ID,null);
            this.getDbConnection().close();

            return String.valueOf(id);

        }catch (Exception e){

            return "0";

        }

    }


}


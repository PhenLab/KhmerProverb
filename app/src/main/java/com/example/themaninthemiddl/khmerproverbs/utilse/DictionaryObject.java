package com.example.themaninthemiddl.khmerproverbs.utilse;

import io.realm.RealmObject;

/**
 * Created by phen on 6/24/2017.
 */
public class DictionaryObject extends RealmObject {

    private String word;
    private String definition;
    private String detail;
    private String direct;
    private String indirect;
    private String id;
    private String is_favorite;
    private String is_add;

    public DictionaryObject() {
    }

    public String getIs_add() {
        return is_add;
    }

    public void setIs_add(String is_add) {
        this.is_add = is_add;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(String is_favorite) {
        this.is_favorite = is_favorite;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDirect() {
        return direct;
    }

    public void setDirect(String direct) {
        this.direct = direct;
    }

    public String getIndirect() {
        return indirect;
    }

    public void setIndirect(String indirect) {
        this.indirect = indirect;
    }

    public DictionaryObject(String id, String word, String definition, String detail, String direct, String indirect, String is_favorite, String is_add) {

        this.word = word;
        this.definition = definition;
        this.detail=detail;
        this.direct=direct;
        this.indirect=indirect;
        this.id=id;
        this.is_favorite=is_favorite;
        this.is_add=is_add;


    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }
}

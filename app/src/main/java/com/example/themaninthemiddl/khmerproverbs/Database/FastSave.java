package com.example.themaninthemiddl.khmerproverbs.Database;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Map;

public class FastSave {

    private static FastSave instance;
    private static SharedPreferences sharedPreferences;

    public static void init(Context context){

        sharedPreferences = context.getSharedPreferences("config", Context.MODE_PRIVATE);

    }

    public static FastSave getInstance(){
        if (instance == null){
            validateInitialization();
            synchronized (FastSave.class){
                instance = new FastSave();
            }
        }
        return instance;
    }
    private static void validateInitialization() {
        if (sharedPreferences == null)
            throw new FastSaveException("FastSave Library must be initialized inside your application class by calling FastSave.init(getApplicationContext)");
    }
    public static class FastSaveException extends RuntimeException {

        public FastSaveException(String message) {
            super(message);
        }
    }



    public void saveInt(String key, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getInt(String key) {
        if (isKeyExists(key)) {
            return sharedPreferences.getInt(key, 0);
        }
        return 0;
    }

    public void saveBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        if (isKeyExists(key)) {
            return sharedPreferences.getBoolean(key, false);
        } else {
            return false;
        }
    }


    public void saveFloat(String key, float value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public float getFloat(String key) {
        if (isKeyExists(key)) {
            return sharedPreferences.getFloat(key, 0.0f);
        }
        return 0.0f;
    }


    public void saveLong(String key, long value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public long getLong(String key) {
        if (isKeyExists(key)) {
            return sharedPreferences.getLong(key, 0);
        }
        return 0;
    }


    public void saveString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getString(String key) {
        if (isKeyExists(key)) {
            return sharedPreferences.getString(key, null);
        }
        return null;
    }
    public String getString(String key, String defaultValue) {
        if (isKeyExists(key)) {
            return sharedPreferences.getString(key, defaultValue);
        }
        return defaultValue;
    }

    public void clearSession() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public boolean deleteValue(String key) {
        if (isKeyExists(key)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(key);
            editor.apply();
            return true;
        }

        return false;
    }



    public boolean isKeyExists(String key) {
        Map<String, ?> map = sharedPreferences.getAll();
        if (map.containsKey(key)) {
            return true;
        } else {
            Log.e("FastSave", "No element founded in sharedPrefs with the key " + key);
            return false;
        }
    }
}

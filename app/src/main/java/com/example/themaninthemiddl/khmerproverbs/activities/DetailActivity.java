package com.example.themaninthemiddl.khmerproverbs.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.themaninthemiddl.khmerproverbs.Database.DbBackend;
import com.example.themaninthemiddl.khmerproverbs.R;
import com.example.themaninthemiddl.khmerproverbs.font.CustomTypefaceSpan;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView toolbarTitle;
    private Toolbar toolbar;
    Typeface tittleFont;
    private TextView tvDefinition, tvHeadDefinition, tvProverb;
    private TextView tvDetail, tvHeadDetail;
    private TextView tvDirect, tvHeadDirect;
    private TextView tvIndirect,tvHeadIndirect;
    Typeface textFont;
    private ImageView imgFavorite, imgDelete;
    private String is_favorite ="0";
    private DbBackend dbBackend;
    private String table_name ="", id ="";
    private String is_add ="0";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        tittleFont = Typeface.createFromAsset(getAssets(),"fonts/KhmerOSmuol.ttf");
        textFont = Typeface.createFromAsset(getAssets(),"fonts/KhmerOS.ttf");

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.proverb_detail));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbBackend = new DbBackend(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        ///-----Apply text font-------/////
        applyFontForToolbarTitle(this);
        tvProverb = (TextView)findViewById(R.id.proverb);
        tvHeadDefinition = (TextView)findViewById(R.id.head_definition);
        tvDefinition = (TextView)findViewById(R.id.definition);
        tvHeadDirect = (TextView)findViewById(R.id.head_direct);
        tvDirect = (TextView)findViewById(R.id.direct_container);
        tvDetail = (TextView)findViewById(R.id.detail_container);
        tvHeadDetail = (TextView)findViewById(R.id.head_detail);
        tvHeadIndirect = (TextView)findViewById(R.id.head_indirect);
        tvIndirect = (TextView)findViewById(R.id.indirect_container);
        imgFavorite = (ImageView)findViewById(R.id.add_favorite);
        imgFavorite.setOnClickListener(this);


        applyFontForTextView();


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        String Word=bundle.getString("word");
        String Definition=bundle.getString("definition");
        String Direct=bundle.getString("direct");
        String Indirect=bundle.getString("indirect");
        String Detail=bundle.getString("detail");
        is_favorite=bundle.getString("is_favorite");
        table_name=bundle.getString("table_name");
        id=bundle.getString("id");
        is_add =bundle.getString("is_add");



        //check if favorite or not
        changeFavoriteImg(is_favorite);


        tvProverb.setText(Word);

        Log.i("Detail",Detail+",");
        try {

            if (Detail.equals("NULL") || Detail.equals("null") || Detail.equals("")) {
                tvDetail.setVisibility(View.GONE);
            } else
                tvDetail.setText(Detail);
        }catch (Exception e){
            tvDetail.setVisibility(View.GONE);
        }
        Log.i("Definition",Definition+",");
        try {
            if(Definition.equals("NULL") || Definition.equals("null") || Definition.equals("")){
                tvHeadDefinition.setVisibility(View.GONE);
                tvDefinition.setVisibility(View.GONE);
            }else
                tvDefinition.setText(Definition);

        }catch (Exception e){
            tvHeadDefinition.setVisibility(View.GONE);
            tvDefinition.setVisibility(View.GONE);
        }

        Log.i("Direct",Direct+",");
        try {
            if (Direct.equals("NULL") || Direct.equals("null") || Direct.equals("")) {
                tvHeadDirect.setVisibility(View.GONE);
                tvDirect.setVisibility(View.GONE);
            } else
                tvDirect.setText(Direct);
        }catch (Exception e){

            tvHeadDirect.setVisibility(View.GONE);
            tvDirect.setVisibility(View.GONE);

        }

        Log.i("Indirect",Indirect+",");
        try {

            if (Indirect.equals("NULL") || Indirect.equals("null") || Indirect.equals("")) {
                tvHeadIndirect.setVisibility(View.GONE);
                tvIndirect.setVisibility(View.GONE);
            } else
                //word.setText(Word);
                tvIndirect.setText(Indirect);
        }catch (Exception e){
            tvHeadIndirect.setVisibility(View.GONE);
            tvIndirect.setVisibility(View.GONE);
        }

    }

    private void changeFavoriteImg(String is_favorite) {


        try {
            if (is_favorite.equals("1")) {

                imgFavorite.setImageResource(R.drawable.ic_favorited);
            }
            else {
                imgFavorite.setImageResource(R.drawable.ic_not_favorite);

            }
        }catch (Exception e){
            is_favorite = "1";
            Log.d("error_check","msg"+is_favorite);

        }
    }

    private void applyFontForTextView() {
        tvProverb.setTypeface(tittleFont, Typeface.BOLD);
        tvHeadDefinition.setTypeface(textFont, Typeface.NORMAL);
        tvDefinition.setTypeface(textFont, Typeface.NORMAL);
        tvDetail.setTypeface(textFont, Typeface.NORMAL);
        tvHeadDirect.setTypeface(textFont, Typeface.NORMAL);
        tvDirect.setTypeface(textFont, Typeface.NORMAL);
        tvHeadDetail.setTypeface(textFont, Typeface.NORMAL);
        tvHeadIndirect.setTypeface(textFont, Typeface.NORMAL);
        tvIndirect.setTypeface(textFont, Typeface.NORMAL);
    }

    public void applyFontForToolbarTitle(Activity context){
        Toolbar toolbar = (Toolbar) context.findViewById(R.id.toolbar);
        for(int i = 0; i < toolbar.getChildCount(); i++){
            View view = toolbar.getChildAt(i);
            if(view instanceof TextView){
                TextView tv = (TextView) view;
                if(tv.getText().equals(toolbar.getTitle())){
                    tv.setTypeface(tittleFont);
                    break;
                }
            }
        }
    }


    @Override
    public void onClick(View view) {


        switch (view.getId()){

            case R.id.add_favorite:

                try{
                    if (is_favorite.equals("1") || is_favorite.equals(1)) {

                        is_favorite = "0";
                    }
                    else
                        is_favorite = "1";
                }
                catch (Exception e) {

                    is_favorite = "1";
                }

                changeFavoriteStatus(is_favorite);
                changeFavoriteImg(is_favorite);
                break;

        }
    }

    public void changeFavoriteStatus(String is_favorite){

        dbBackend.changeFavoriteStatus(is_favorite,table_name, id);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        try{
            if (is_add.equals("1")) {
                MenuInflater menuInflater = getMenuInflater();
                menuInflater.inflate(R.menu.mnu_actions, menu);

                for (int i = 0; i < menu.size(); i++) {
                    MenuItem mi = menu.getItem(i);

                    //for aapplying a font to subMenu ...
                    SubMenu subMenu = mi.getSubMenu();
                    if (subMenu != null && subMenu.size() > 0) {
                        for (int j = 0; j < subMenu.size(); j++) {
                            MenuItem subMenuItem = subMenu.getItem(j);
                            applyFontToMenuItem(subMenuItem);
                        }
                    }

                    //the method we have create in activity
                    applyFontToMenuItem(mi);
                }
            }
        }
        catch (Exception e){

        }
        return super.onCreateOptionsMenu(menu);
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/KhmerOS.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){

            case R.id.edit:

                Intent intent = new Intent(this, EditProverbActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
                break;

            case R.id.delete:

                deleteProverb(id);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void deleteProverb(String ID){


        if (dbBackend.deleteNewProverbById(ID).equals("success")){
            this.finish();
            Toast.makeText(this, getString(R.string.delete_success), Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(this, getString(R.string.delete_error), Toast.LENGTH_SHORT).show();


    }
}
